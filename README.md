# Literaturliste für SS19 ABC2
## Vorwort
Literaturliste für das SS19 ABC2 an der HS Aalen, Verwendung auf eigene Gefahr!!!
Keine Haftung für aus der Verwendung resultierenden Verletzungen (Kopfschmerzen, abgetrennte Körperteile oder ähnliches).
Tipp: Die Schattenbibliothek libgen.io enthält eine unglaubliche Vielfalt an Literatur.

## Zu den Downloads
Der Download über "SpringerLink" oder ist nur aus authorisierten Umgebungen (wie z.B. dem Netzwerk der Hochschule) möglich.

## Methoden der Strukturbestimmung
### Massenspektrometrie (Neusüß)
>>>
1. Ionization
    1. EI, APCI, APPI, PTR, SIFT
    2. ESI (MALDI)
2. Mass Analyzer
    1. Low resolution (Q, IT) vs. High resolution (TOF, Orbitrap, FTICR)
    2. Fragmentierung
3. Isotope Mass Spectrometry
4. Ion Mobility - Mass Spectrometry
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
|Bioanalytik|Friedrich Lottspeich||[springer.com](https://www.springer.com/de/book/9783827429421)|NA|
|Lehrbuch der Quantitativen Analyse|Daniel C. Harris||[springer.com](https://www.springer.com/de/book/9783642377877)|[SpringerLink](http://link.springer.com/978-3-642-37788-4), [libgen](http://libgen.io/item/index.php?md5=60FA83E20794749EFE440F8437C3DB6D)|
|Instrumentelle Analytik|Douglas A. Skoog, James F. Holler, Stanley R. Crouch||[springer.com](https://www.springer.com/de/book/9783642381690)|[libgen](http://libgen.io/item/index.php?md5=E25BC88F8A05FC856D6C52CF3B6447AD)|
|Instrumentelle Analytische Chemie|Karl Cammann (Hrsg.)||[springer.com](https://www.springer.com/de/book/9783827427397)|NA|
|Mass Spectrometry|Jürgen H. Gross||[springer.com](https://www.springer.com/de/book/9783319543970)|[SpringerLink](http://link.springer.com/978-3-319-54398-7), [libgen](http://libgen.io/item/index.php?md5=74E26CE40925AA3BA903C65DA43488C9)|
|Mass Spectrometry: Principles and Applications, 3rd Edition|Edmond de Hoffmann, Vincent Stroobant||[wiley.com](https://www.wiley.com/en-us/Mass+Spectrometry%3A+Principles+and+Applications%2C+3rd+Edition-p-9780470033104)|[libgen](http://libgen.io/book/index.php?md5=323CFABE6B2B792086631955431E4C4A)|
|Introduction to Mass Spectrometry: Instrumentation, Applications, and Strategies for Data Interpretation, 4th Edition|J. Throck Watson O. David Sparkman||[wiley.com](https://www.wiley.com/en-us/Introduction+to+Mass+Spectrometry%3A+Instrumentation%2C+Applications%2C+and+Strategies+for+Data+Interpretation%2C+4th+Edition-p-9780470516348)|[libgen](http://libgen.io/item/index.php?md5=C0B9BDEAA4E7FAF0B1EF80FAEE77715A)|

### Fortgeschrittene NMR-Methoden (Junker)
>>>
1. Theoretische und Experimentelle Grundlagen
    1. Prinzip der Kernresonanz
    2. Chemische Verschiebung
    3. Kopplung durch Bindungen
    4. Integration & Auswertung
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
|Ein- und zweidimensionale NMR-Spektroskopie|Horst Friebolin||[wiley-vch.de](https://www.wiley-vch.de/de/fachgebiete/naturwissenschaften/ein-und-zweidimensionale-nmr-spektroskopie-978-3-527-33492-6)|[libgen](http://libgen.io/item/index.php?md5=F18FEF90C40D51A5AABAE0ED5F140460)|
|Spektroskopische Methoden in der organischen Chemie|Stefan Bienz, Laurent Bigler, Thomas Fox, Herbert Meier||[thieme.de](https://www.thieme.de/shop/Chemie-Allgemein/Bienz-Bigler-Fox-Meier-Spektroskopische-Methoden-in-der-organischen-9783135761091/p/000000000138640109)|[libgen](http://libgen.io/book/index.php?md5=4F12DE8E42AD79F89D7A23B37DC1DA56)|

## Biopharmazeutische Analytik
>>>
1. ...
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
||||||

### Biopharmazeutika (Stein)
>>>
1. Definition
2. Operating frameworks 'biosimilars' EU and USA (EMA and FDA)
3. Biopharmaceutical Market
4. Production of Biopharmaceuticals
    1. Recombinant Products
    2. Fermentation (Biotechnology)
    3. Feeding Train
    4. Downstream Train
5. Antibodies: Prototype of ideal Biopharmaceuticals
6. Examples of Biopharmaceuticals
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
||||||

## Nucleinsäureanalytik (Schnell)
>>>
1. Die Entschlüsselung der DNA
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
|Bioanalytik|Friedrich Lottspeich||[springer.com](https://www.springer.com/de/book/9783827429421)|NA|
||||||

## Medizinische Chemie (Schaschke)
>>>
1. Medizinische Chemie - ein Überblick
2. Grundlagen der Arzneistoffentwicklung
    1. Galenik
    2. Pharmakokinetik
    3. Pharmakodynamik
3. Wirkstoffentwicklung
    1. Die Suche nach der Leitstruktur
    2. Experimentelle und theoretische Methoden
4. Fallbeispiele
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
|Wirkstoffdesign| Gerhard Klebe ||[springer.com](https://www.springer.com/de/book/9783827422132)|[SpringerLink](http://link.springer.com/978-3-8274-2213-2), [libgen](http://libgen.io/book/index.php?md5=E807D173A62BFD1CA4CB138BCA629044)|
||||||

## Umweltanalytik (Neusüß)
>>>
1. ...
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
||||||

## Instrumentelle Organische Analytik (Neusüß)
>>>
1. ...
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
||||||

## Chemometrie (Flottmann)
>>>
1. Basics and hypothesis testing
2. ANOVA
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
|Optimal Design of Experiments: A Case Study Approach|Peter Goos, Bradley Jones||[wiley.com](https://www.wiley.com/en-us/Optimal+Design+of+Experiments%3A+A+Case+Study+Approach-p-9780470744611)|[libgen](http://libgen.io/item/index.php?md5=A688FF0CAF465B3CBA70B3B3ABC176AD)|
|Statistical Methods in Analytical Chemistry|Peter C. Meier Richard E. Zünd||[wiley.com](https://onlinelibrary.wiley.com/doi/book/10.1002/0471728411)|[libgen](http://libgen.io/item/index.php?md5=0BB6118F7A1405CC3B5FE7E8ED23EA2F)|
|Statistik für Anwender|Wolfgang Gottwald||[books.google.de](https://books.google.de/books/about/Statistik_f%C3%BCr_Anwender.html?id=FRvXQgAACAAJ&redir_esc=y)|NA|
|Measurement, Statistics and Computation|David McCormick, Alan Roach||[books.google.de](https://books.google.de/books/about/Measurement_Statistics_and_Computation.html?id=Un_0bxOWpKsC&redir_esc=y)|NA|


## hemie der Peptide und Peptidomimetika (Schaschke)
>>>
1. Peptide als Arzneistoffe
2. Grundlegende peptidomimetische Konzepte
3. Identifizierung biologisch aktiver Peptid-Core-Strukturen
4. Spezielle Gebiete der Festphasensynthese
5. Ausgewählte Fallbeispiele
>>>

| Titel | Autor | Nützlich in Kapitel | Website | Download |
|:------|:------|:--------------------|:--------------|:---------|
||||||

# Der Vorlesungsplan
![Vorlesungsplan](img/Vorlesungsplan_SOSE19.jpg)
[PDF](img/Vorlesungsplan_SOSE19.pdf)  
(https://www.hs-aalen.de/de/pages/vorlesungsplan)